/* EJERCICIO: Le pasamos a una función dos 
números y nos devuelve el mayor de ellos */

function mayor(a,b){
    var mayor = a;
    if (b > a) {
        mayor = b; 
        return b;
    }
    else return a;
}
// llamando a la función:
// console.log("El mayor es: " + mayor(12,13));


// EJERCICIO: muestra en pantalla datos referidos al número recibido
function datos(numero){
    if (numero % 2 == 0) {
        console.log(numero + " SÍ es par")
    } else {
        console.log(numero + " NO es par")
    }
    
    if (numero % 3 !== 0) {
        console.log(numero + " NO es divisible por 3");
    } else {
        console.log(numero + " SÍ es divisible por 3")
    }

    if (numero % 5 == 0) {
        console.log(numero + " SÍ es divisible por 5")
    } else {
        console.log(numero + " NO es divisible por 5")
    }

    if (numero % 7 !== 0) {
        console.log(numero + " NO es divisible por 7")
    } else {
        console.log(numero + " SÍ es divisible por 7")
    }
}
// llamando a la función:
// datos(70);


// EJERCICIO: Recibe un array de valores numéricos, devuelve la suma de todos ellos
function sumaValores(arr){
    var total = 0;
    for (i of arr){
        total += i;
    }
    return total;
}
//Definiendo Array: 
// var array = [1,3,9,2,3];
// llamando a la función:
// console.log(sumaValores(array));


// EJERCICIO: Devuelve el producto factorial del número recibido
function factorial(x){
    if (x == 1){
        return 1;
    } else {
        return x * (factorial (x-1));
    }
}
// llamando a la función:
// console.log(factorial(10));


// EJERCICIO: convierte primera letra enmayúscula, resto en minúscula
function capitaliza(x){
    return x.charAt(0).toUpperCase() + x.slice(1);
}
// llamando a la función:
// console.log(capitaliza("barcelona"));


/* EJERCICIO: Indicar la longitud de la palabra, si su longitud es par o 
impar e indicar las vocales y consonantes que tiene */
function palabra(x){
    var vocales = 0;
    var consonantes = 0;
    console.log("'" + x + "'" + " tiene " + x.length + " letras");
    if (x.length % 2 == 0){
        console.log(x.length + "es un número par")
    } else{
        console.log(x.length + " es un número impar")
    }
    for (i of x){
        if (i == "a" || i == "e" || i == "i" || i == "o" || i == "u" ){
            vocales++;
        } else {
            consonantes ++;
        }
    }
    console.log("Vocales: " + vocales);
    console.log("Consonantes: " + consonantes);
}
// llamando a la función:
// palabra("Barcelon");


//EJERCICIO: Crear una función que al llamarla nos diga el día que es hoy
function hoy(){
    diasSemana = ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"];
    dia = new Date();
    return ("Hoy es " + diasSemana[dia.getDay()]);
}
// llamando a la función
//console.log(hoy());


//EJERCICIO: Crear una función que al llamarla nos diga los días que quedan para navidad
function navidad(){
    navidad = new Date("2019,12,25");
    hoy = Date.now();
    // diasRestantes = navidad-hoy ;         
    console.log("Quedan " + ((navidad-hoy)/1000/60/60/24).toFixed(2) + " días para navidad");
}
//llamando a la función
//navidad();


//EJERCICIO: De un array sumar todos los números de dentro e indicar el máx. y el mín.
function analiza(array){
    suma = 0;
    max = array[0];
    min = array[0];
    for (i of array){
        if (i > max){
            max = i;
        }
        if (i < min){
            min = i;
        }
        suma += i;
    }
    console.log("La suma es: " + suma);
    console.log("El número mayor es: " + max);
    console.log("El número menor es: " + min);  
}
/* //Creando array: 
array = [2,4,8,-2];
//Iniciando la función:
analiza(array); */


//EJERCICIO: Fibonacci
function fibonacci(num){
    if(num == 0 || num==1)
        return num;
    else{
        return fibonacci(num-1) + fibonacci(num-2);
    }
}

//llamando a la función
/*
numero = prompt("Inserta un número");
for (var i = 0; i < numero; i++){
            console.log(fibonacci(numero) + ", ");
}*/