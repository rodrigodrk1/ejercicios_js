// INICIO
function bucle01() {
  var total = 0;

  do {
    num = prompt("Entra número") * 1;
    total = total + num;
  } while (num !== 0);

  console.log(total);
}

function bucle02() {
  var total = 0;
  var num;
  var contador = 0;
  var max = 0;
  var min = 1000;

  do {
    num = prompt("Inserta número")*1;
    if (num > max) {
      max = num;
    }
    if (num !== 0) { 
      if (num < min) {
        min = num;
      }
      total += num;
      contador++;
    }
  } while (num !== 0);
  console.log("Total: ", total);
  console.log("Media: ", total / contador);
  console.log("Mínimo: ", min);
  console.log("Máximo: ", max);
}